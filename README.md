# ESPRESSO Radial Velocity Data Translator

This repository contains a Python-based translator that converts radial velocity data products obtained with the **ESPRESSO** spectrograph and **DRS** into a standardized L2 format

## Table of Contents
- [Requirements](#requirements)
- [Usage](#usage)
- [Example](#example)

## Requirements

To run the translator, you will need the following input files:
- The **raw** file of the measurement (ESPRE.YYYY-MM-DDTHH:MM:SS.ms.fits)
- **s2d** products for fiber A and fiber B (r.ESPRE.YYYY-MM-DDTHH:MM:SS.ms_S2D_BLAZE_A.fits & r.ESPRE.YYYY-MM-DDTHH:MM:SS.ms_S2D_BLAZE_B.fits)
- **blaze** files for fiber A and fiber B (r.ESPRE.YYYY-MM-DDTHH:MM:SS.ms_BLAZE_A.fits & r.ESPRE.YYYY-MM-DDTHH:MM:SS.ms_BLAZE_B.fits)

## Usage
To use the translator, simply import the file ESPRRV2 file in your code and follow the steps below.
## Example

```python
import ESPRRV2 as lv2
input_dir='data/'
output_dir = ''
raw_file = 'ESPRE.2017-11-28T01:14:08.269.fits'

translator = lv2.ESPRESSORV2(input_dir = input_dir, raw_file =raw_file, output_dir = output_dir)
translator.do_conversion()
translator.save()
```

The newly created L2 file will be saved in the specified output directory. The name of the new file is the same as that of the raw file, with a "_L2" appended at the end.