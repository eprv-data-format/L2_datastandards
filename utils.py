from astroquery.simbad import Simbad

def get_gaia_name(obj):
        """Query the Simbad database to get the Gaia name of the object.

        Args:
            obj (str): the name of the object

        Returns:
            str: the Gaia identifier of the object in the format "Gaia DR2 1234567890"
        """
        try:
            #We query the object in the Simbad database
            result_table = Simbad.query_objectids(obj)
            #We iterate over the results to find the Gaia identifier
            for name in result_table:
                if(name[0].lower().startswith('gaia')):
                    
                    return name[0]
        except:
            return 'None'
        return

def convert_to_sexagesimal(ra, dec):
        """This function makes the conversion between the ESPRESSO drs format for the right ascension and declination to the sexagesimal format.

        Args:
            ra (float): The right ascension obtained from the TARG ALPHA keyword of the header
            dec (float): The declination obtained from the TARG DELTA keyword of the header

        Returns:
            tuple of string: The right ascension and declination in the sexagesimal format
        """
        ra = f'{ra:10.3f}'
        ra = ra[:2] + 'h' + ra[2:4] + 'm' + ra[4:] + 's'
        dec = f'{dec:011.3f}' if dec < 0 else f'{dec:10.3f}'
        if dec.startswith('-'):

            dec = dec[:3] + 'd' + dec[3:5] + 'm' + dec[5:] + 's'

        else:

            dec = dec[:2] + 'd' + dec[2:4] + 'm' + dec[4:] + 's'

        return ra, dec

def compute_hour_angle(lst, ra):
        """Computes the hour angle from the local sidereal time and the right ascension of the object.

        Args:
            lst (str): local sideral time in the format hh:mm:ss.ms
            ra (ra): right ascension of the object in the format hh:mm:ss.ms

        Returns:
            str: the hour angle in the format hh:mm:ss.ms
        """
        h_ra = int(ra[:ra.find('h')])
        m_ra = int(ra[ra.find('h')+1:ra.find('m')])
        s_ra = float(ra[ra.find('m')+1:ra.find('s')])

        h_lst = int(lst[:lst.find('h')])
        m_lst = int(lst[lst.find('h')+1:lst.find('m')])
        s_lst = float(lst[lst.find('m')+1:lst.find('s')])
        ha_s = 3600*(h_lst - h_ra) + 60*(m_lst - m_ra) + (s_lst - s_ra)
        return convert_lst(ha_s)

def convert_lst(lst):
        """Converts the local sidereal time from seconds to hh:mm:ss.ms

        Args:
            lst (foat): the local sidereal time in seconds

        Returns:
            str: the local sidereal time in the format hh:mm:ss.ms
        """
        if(lst < 0):
            res = convert_lst(-lst)
            return '-' + res
        hours = int(lst//3600)
        minutes = int((lst %3600)//60)
        seconds = int(lst %60)
        ms = round(lst % 1 * 1000)
        
        return f"{hours:02}h{minutes:02}m{seconds:02}.{ms:03}s"

def doppler_shift(wave, rv):
        """Performs the doppler shift on the wavelength values.

        Args:
            wave (numpy array): the original wavelength values
            rv (radial velocity): the radial velocity of the object in km/s

        Returns:
            numpy array: the doppler shifted wavelength values
        """
        c = 299792.458 #speed of light in km/s
        return wave + wave *rv/c   