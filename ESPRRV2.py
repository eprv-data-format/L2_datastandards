from astropy.io import fits
import astropy.units as u
from astropy.nddata import VarianceUncertainty
from specutils import SpectrumCollection
import numpy as np
import config
from astropy.time import Time
import pandas as pd
from astroquery.simbad import Simbad    
import utils

class ESPRESSORV2:
    def __init__(self, input_dir = '', output_dir = '', raw_file = None, file_format = None):
        """Initializes the ESPRESSORV2 object.

        Args:
            input_dir (str, optional): The path to the folder where the original data is located. Defaults to the current folder.
            output_dir (str, optional): The path to the output folder where the data will be saved. Defaults to ''.
            raw_file (str, optional): The file name of the raw file. Defaults to None.
            file_format (str, optional): Either 'L2' or 'original'. With 'L2', the data is separated by slices and cameras. Defaults to None. If not stated, the value in config.data_format will be used.
        """
        self.input_dir = input_dir 
        self.output_dir = output_dir
        self.raw_file = raw_file
        self.file_format = file_format if file_format is not None else config.data_format
        self.blaze_file = []
        self.l2 = fits.HDUList()
        return
    
    
    def do_conversion(self):
        """
        Convert the raw file to a level 2 file by converting the e2ds, blaze, raw file information and adding the L2 header.
        """
        self.convert_e2ds()
        self.convert_blaze()
        self.convert_rawfile()

        self.create_l2_header()

        return
    
    
    
    def create_l2_header(self):
        """Creates the L2 header by copying the information from the raw file and adding the necessary information for the L2 file.
        """
        #We create an empty HDU to store the L2 header
        l2_hdu = fits.ImageHDU(data = None)
        l2_hdu.header['EXTNAME'] = 'L2_HEADER'
        #We load the header map to convert between raw file headers and L2 header
        header_map = pd.read_csv('/Users/emilio/Desktop/EPRV/EPRV_lv2/header_map.csv')
        #We replace the %UT% in the header map with the front end ID to get the correct keywords depending on which UT was used
        header_map['ESO_keyword'] = header_map['ESO_keyword'].str.replace('%UT%', self.UT)
        #Special case for UT1
        if self.UT == '1':
            header_map['ESO_keyword'] = header_map['ESO_keyword'].str.replace('INS1', 'INS')
        #We iterate over all the keys of the header map
        for index, values in header_map.iterrows():
            if(header_map.skip.iloc[index] == 1):
                continue
            #Add the HIERARCH keyword to the header if the keyword is longer than 8 characters
            if(len(values[0]) > 8):
                values[0] = 'HIERARCH ' + values[0]
            values[0] = values[0].strip()

            try:
                #If there is a fixed value to set, we set it
                if(pd.notna(header_map.value.iloc[index])):
                    l2_hdu.header[values[0]] = header_map.value.iloc[index]
                #Otherwise, we copy the value from the raw file
                elif(pd.notna(header_map['ESO_keyword'].iloc[index])):
                    l2_hdu.header[values[0]] = (self.l2['PRIMARY'].header[header_map['ESO_keyword'].iloc[index]], self.l2['PRIMARY'].header.comments[header_map['ESO_keyword'].iloc[index]].strip())
                #If the value is not present in the raw file, we set it to None
                else:
                    l2_hdu.header[values[0]] = None
            except Exception as e:
                print(e)
        

        l2_hdu.header['FILENAME'] = ('r.'+self.raw_file[:-5] + '_L2.fits', 'Name of the file')

        #Getting Gaia identifier
        try:
            gaia_name = utils.get_gaia_name(self.l2['PRIMARY'].header['OBJECT'])
            l2_hdu.header['CATSRC'] = (gaia_name[:9],'Catalog name')
            l2_hdu.header['CATID'] = (gaia_name[9:], 'Catalog ID')

        except Exception as e:
            print('Could not find Gaia name: ' + str(e))

        current_time = Time.now()
        l2_hdu.header['DATE']  = (current_time.iso, 'Date of file creation')
        l2_hdu.header['TCSZA'] = (np.round(90 - l2_hdu.header['TCSEL'], 3), '[deg] Zenith angle')

        #Converting LST in seconds to hh:mm:ss.ms
        lst = utils.convert_lst(l2_hdu.header['TCSLST'])
        l2_hdu.header['TCSLST'] = (lst, 'Local Sidereal Time')
        ra, dec = utils.convert_to_sexagesimal(l2_hdu.header['CATRA'], l2_hdu.header['CATDEC'])
        l2_hdu.header['CATRA'] = (ra, 'Catalog Right Ascension')
        l2_hdu.header['CATDEC'] = (dec, 'Catalog Declination')
        tcs_ra, tcs_dec = utils.convert_to_sexagesimal(l2_hdu.header['TCSRA'], l2_hdu.header['TCSDEC'])
        l2_hdu.header['TCSRA'] = (tcs_ra, 'Telescope Right Ascension')
        l2_hdu.header['TCSDEC'] = (tcs_dec, 'Telescope Declination')
        l2_hdu.header['TCSHA'] = (utils.compute_hour_angle(lst, tcs_ra), 'Hour angle')
        l2_hdu.header['CATPMRA'] = (l2_hdu.header['CATPMRA']*1000, '[marcsec/yr] Proper Motion Alpha')
        l2_hdu.header['CATPMDEC'] = (l2_hdu.header['CATPMDEC']*1000, '[marcsec/yr] Proper Motion Delta')    
        self.l2.insert(1, l2_hdu)
        return
    
    def convert_rawfile(self):
        """Transfer the raw file information to the level 2 hdul file by keeping the same primary header
        and copying the exposure meter, pupil image and guiding frame information.

        """
        
        with fits.open(self.input_dir + self.raw_file) as hdul:
            prim_header = hdul['PRIMARY'].header.copy()
            
            prim_header_l2 = fits.PrimaryHDU(header=prim_header)
            prim_header_l2.header['EXTEND'] = True
            self.l2.insert(0, prim_header_l2)
            try:
                if('HIERARCH ESO OCS ENABLED FE' in hdul['PRIMARY'].header):
                    front_end_id  = hdul['PRIMARY'].header['HIERARCH ESO OCS ENABLED FE']
                elif('TELESCOP' in hdul['PRIMARY'].header):
                    front_end_id = hdul['PRIMARY'].header['TELESCOP'][-1]
                self.UT = front_end_id
                pupil_key = 'PS' + str(front_end_id)
                guiding_key = 'FS' + str(front_end_id) + 'INT'
            except Exception as e: 
                print('Could not find front end ID ' + str(e))
                
            try:
                exp_meter_hdu = hdul['Exp Meter bin table'].copy()
                exp_meter_hdu.header['EXTNAME'] = 'EXPOMETER'
            except Exception as e:
                print('Error when converting exposure meter: ' + str(e))
                exp_meter_hdu = fits.ImageHDU(data = None)
                exp_meter_hdu.header['EXTNAME'] = 'EXPOMETER'
            try:
                pupil_img_hdu = hdul[pupil_key].copy()
                pupil_img_hdu.header['EXTNAME'] = 'PUPILIMAGE'
            except Exception as e:
                print('Error when converting pupil image: ' + str(e))
                pupil_img_hdu = fits.ImageHDU(data = None)
                pupil_img_hdu.header['EXTNAME'] = 'PUPILIMAGE'
            try:
                guiding_frame_hdu = hdul[guiding_key].copy()
                guiding_frame_hdu.header['EXTNAME'] = 'GUIDINGIMAGE'
            except Exception as e: 
                print('Error when converting guiding image: ' + str(e))
                guiding_frame_hdu = fits.ImageHDU(data = None)
                guiding_frame_hdu.header['EXTNAME'] = 'GUIDINGIMAGE'

            self.l2.extend([exp_meter_hdu, pupil_img_hdu, guiding_frame_hdu] )
        return 
    def create_QC_header(self, header):
        """Create a QC header by removing the ESO QC cards from the primary header and adding them to a new header.

        Args:
            header (fits.Header): The primary header of one of the s2d files produced by the espresso DRS

        Returns:
            fits.Header: The header containing all the ESO QC cards from the primary header
        """
        temp_head = fits.Header()
        to_remove = [c for c in header.keys() if "ESO QC" in c]
        qc_cards = [(key, header[key], header.comments[key]) for key in to_remove]
        for key, value, comment in qc_cards:
            header.remove(key)
            
            if(len(key) > 8):
                key = 'HIERARCH ' + key.strip()
            #Special case as the ESPRESSO drs sometimes produces values that are too long
            if(len(str(value)) > 20):
                if(type(value) == float):
                    value = float(str(value)[:20])
                else:
                    value = str(value)[:20]
            #Special case for the slope_x keyword that would raise a warning because of its length
            if(key == 'HIERARCH ESO QC DRIFT DET0 SLOPE_X' or key == 'HIERARCH ESO QC DRIFT DET1 SLOPE_X'):
                comment = comment.replace('for', '')
            temp_head[key.strip()] = (value, comment.strip())
            
        return header, temp_head
    def convert_e2ds(self):
        """Converts the e2ds files to the level 2 format by extracting the values of the e2ds files and adding them to the level 2 file.
        """
        #Loop over the two fibers (science and FP)
        for i, fiber in enumerate(config.fibers.keys()):
            #Load the s2d file
            e2ds_file = self.input_dir + 'r.' +self.raw_file[:-5] + '_S2D_BLAZE_' + fiber + '.fits'

            with fits.open(e2ds_file) as hdul:
                #
                prim_header = hdul['PRIMARY'].header.copy()
                #We create the QC header and add it to the HDU list
                prim_head, QC_head = self.create_QC_header(prim_header)
                QC_hdu = fits.ImageHDU(data = None, header = QC_head)
                QC_hdu.header['EXTNAME'] = config.fibers[fiber] + '_HEADER'
                self.l2.extend([QC_hdu])

                if(fiber == 'SCI'):
                    blaze_file = prim_header['HIERARCH ESO PRO REC1 CAL28 NAME']
                else:
                    blaze_file = prim_header['HIERARCH ESO PRO REC1 CAL29 NAME']
                self.blaze_file.append(blaze_file)
                #We iterate over the cameras, slices and fields to extract the values from the e2ds file
                if(self.file_format == 'L2'):
                    for camera in config.cam_names.keys():
                        for slice in config.slices:
                            for field in config.extnames.keys():
                                #We extract the values from one invividual camera
                                single_cam_values = hdul[field].data[config.cam_range[camera][0]:config.cam_range[camera][1], :]
                                if(field == 'WAVEDATA_VAC_BARY'):
                                    #We doppler shift the wavelength values
                                    single_slice_bary = single_cam_values[slice::2,:].copy()
                                    hdul_l2_bary = fits.ImageHDU(data = single_slice_bary.copy(), header = hdul[field].header)
                                    hdul_l2_bary.header['EXTNAME'] = config.fibers[fiber]+ config.slice_names[slice] + config.cam_names[camera] + '_WAVE_BARY'

                                    single_cam_values = utils.doppler_shift(single_cam_values, QC_head['HIERARCH ESO QC BERV'])
                                    bary_data = np.ones(single_cam_values.shape)*QC_head['HIERARCH ESO QC BERV']
                                    single_slice_berv = bary_data[slice::2,:]
                                    berv_hdu = fits.ImageHDU(data = single_slice_berv.copy())
                                    berv_hdu.header['EXTNAME'] = config.fibers[fiber]+ config.slice_names[slice] + config.cam_names[camera] + '_BERV'

                                    self.l2.extend([hdul_l2_bary, berv_hdu])
                                #We extract the values from one individual slice (every other row in the camera values)
                                single_slice_values = single_cam_values[slice::2,:]
                                #We create a new HDU with the values of the slice and the original header
                                hdu_l2 = fits.ImageHDU(data = single_slice_values.copy(), header = hdul[field].header)

                                hdu_l2.header['EXTNAME'] = config.fibers[fiber]+ config.slice_names[slice] + config.cam_names[camera] + config.extnames[field]
                                self.l2.extend([hdu_l2])
                #If the file format is 'original', we extract the values from the e2ds file without separating slices/cameras
                elif self.file_format == 'original':
                    for field in config.extnames.keys():
                        #We extract the values from one invividual camera
                        temp_data = hdul[field].data
                        if(field == 'WAVEDATA_VAC_BARY'):
                            #We doppler shift the wavelength values
                            hdul_l2_bary = fits.ImageHDU(data = temp_data.copy(), header = hdul[field].header)
                            hdul_l2_bary.header['EXTNAME'] = config.fibers[fiber] + '_WAVE_BARY'

                            temp_data = utils.doppler_shift(temp_data, QC_head['HIERARCH ESO QC BERV'])
                            bary_data = np.ones(temp_data.shape)*QC_head['HIERARCH ESO QC BERV']
                            berv_hdu = fits.ImageHDU(data = bary_data.copy())
                            berv_hdu.header['EXTNAME'] = config.fibers[fiber] + '_BERV'

                            self.l2.extend([hdul_l2_bary, berv_hdu])
                        #We extract the values from one individual slice (every other row in the camera values)
                        #We create a new HDU with the values of the slice and the original header
                        hdu_l2 = fits.ImageHDU(data = temp_data.copy(), header = hdul[field].header)

                        hdu_l2.header['EXTNAME'] = config.fibers[fiber] + config.extnames[field]
                        self.l2.extend([hdu_l2])
                        
        return
    def convert_blaze(self):
        """Copies the blaze calibration files to the level 2 file by extracting the values from the blaze files and adding them to the level 2 file.
        """
        #Loop over the two fibers (science and FP)
        for it, fiber in enumerate(config.fibers.keys()):
            #If file format is L2, we split by cameras and slices
            if(self.file_format == 'L2'):
                try:
                    with fits.open(self.input_dir + self.blaze_file[it]) as hdul:
                        for camera in config.cam_names.keys():
                            for slice in config.slices:
                                blaze = hdul[1].data[config.cam_range[camera][0]:config.cam_range[camera][1], :]
                                single_slice_blaze = blaze[slice::2,:]
                                blaze_hdu_l2 = fits.ImageHDU(data = single_slice_blaze.copy(), header = hdul[1].header)

                                blaze_hdu_l2.header['EXTNAME'] =  config.fibers[fiber]+ config.slice_names[slice] + config.cam_names[camera] +'_BLAZE'
                                self.l2.extend([blaze_hdu_l2])
                except Exception as e:
                    print('Error when converting blaze file: ' + str(e))
            #If file format is original, we extract the values from the blaze file without separating by cameras and slices
            elif self.file_format == 'original':
                try:
                    with fits.open(self.input_dir + self.blaze_file[it]) as hdul:
                        blaze = hdul[1].data
                        blaze_hdu_l2 = fits.ImageHDU(data = blaze.copy(), header = hdul[1].header)

                        blaze_hdu_l2.header['EXTNAME'] =  config.fibers[fiber] +'_BLAZE'
                        self.l2.extend([blaze_hdu_l2])
                except Exception as e:
                    print('Error when converting blaze file: ' + str(e))
        return
    def save(self, output_dir = None):
        """Saves the HDU list to a fits file.

        Args:
            output_dir (str, optional): The path to the folder where the file will be saved. Defaults to None.
        """
        if(self.file_format == 'original'):
            self.l2.writeto(self.output_dir + 'r.'+self.raw_file[:-5] + '_L2_nosplit.fits', overwrite = True, output_verify = 'silentfix+ignore')   
        else:
            self.l2.writeto(self.output_dir + 'r.'+self.raw_file[:-5] + '_L2.fits', overwrite = True, output_verify = 'silentfix+ignore')
        print('File saved to ' + self.output_dir + 'r.'+ self.raw_file[:-5] + '_L2.fits')
        return
